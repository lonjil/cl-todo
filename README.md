After a few discussions in various chat rooms, me and Phoe sat down and made
this list of things we think would be good for the CL ecosystem and community.
Easily many man-years of work. We're doing some of this stuff in the hope
that we can other people will join us.

Easier stuff:

   * UltraSpec
   * Updating the wiki (CLiki)
   * Reviving the CDRs
   * Helping with SICL
   * Actually using McCLIM
   * Tutorials, guides, etc, for modern CL and under explained stuff
       * Common Lisp Cookbook

Harder stuff:

   * Getting authors to do versioning (semver?)
   * Semver support for asdf
   * High quality and complete learning material for CL for individuals and groups (books, lectures, etc)
   * Common Lisp CI platform for most used libraries
   * Common Lisp CI platform for all of Quicklisp
   * Getting some common means of testing CL stuff (ASDF:TEST-SYSTEM is barely enough)
   * Test suite for beach's WSCL (and WSCL actually existing)
   * McCLIM on more platforms
